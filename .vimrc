
"set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
 " alternatively, pass a path where Vundle should install plugins
 "call vundle#begin('~/some/path/here')

 " let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/nerdcommenter'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'jiangmiao/auto-pairs'
Plugin 'junegunn/fzf'
Plugin 'dense-analysis/ale'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'vimwiki/vimwiki'
Plugin 'vim-python/python-syntax'
Plugin 'tiagofumo/vim-nerdtree-syntax-highlight'
Plugin 'scrooloose/syntastic'
Plugin 'tpope/vim-surround'
" Change surrounding marks
Plugin 'severin-lemaignan/vim-minimap'
Plugin 'scrooloose/nerdtree'
" added nerdtree
Plugin 'plasticboy/vim-markdown'
"-------------------------
"Plugin 'morhetz/gruvbox'
Plugin 'haishanh/night-owl.vim'
"Plugin 'kaicataldo/material.vim', { 'branch': 'main' }
"Plugin 'ayu-theme/ayu-vim'
" colorsheme
Plugin 'ap/vim-css-color'
"cheat plugin
Plugin 'dbeniamine/cheat.sh-vim'


"asdsadsad"
" The following are examples of different formats supported.
 " Keep Plugin commands between vundle#begin/end.
 " plugin on GitHub repo
 " Plugin 'tpope/vim-fugitive'
 " plugin from http://vim-scripts.org/vim/scripts.html
 " Plugin 'L9'
 " Git plugin not hosted on GitHub
 " Plugin 'git://git.wincent.com/command-t.git'
 " git repos on your local machine (i.e. when working on your own plugin)
 " Plugin 'file:///home/gmarik/path/to/plugin'
 " The sparkup vim script is in a subdirectory of this repo called vim.
 " Pass the path to set the runtimepath properly.
 " Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
 " Install L9 and avoid a Naming conflict if you've already installed a
 " different version somewhere else.
 " Plugin 'ascenator/L9', {'name': 'newL9'}

 " All of your Plugins must be added before the following line
 call vundle#end()            " required
 filetype plugin indent on    " required
 " To ignore plugin indent changes, instead use:
 "filetype plugin on
 "
 " Brief help
 " :PluginList       - lists configured plugins
 " :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
 " :PluginSearch foo - searches for foo; append `!` to refresh local cache
 " :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
 " see :h vundle for more details or wiki for FAQ
 " Put your non-Plugin stuff after this line
"
"------------------------------------------------------------------------------------------------------------------
" ------------------------------------------------------------------------------------------------------------------

"------------------------------------------------------------------------------------------------------------------
"------------------------------------------------------------------------------------------------------------------
"------------------------------------------------------------------------------------------------------------------
"printing all mapped keys to the file
":redir! > vim_keys.txt
":silent verbose map
":redir END
"---------------------------------------
":verbose map to show in vim
"-------------------------------------------------------------------------------------------------------------------
"To comment out blocks in vim:
" press Esc (to leave editing or other mode)
" hit ctrl+v (visual block mode)
" use the ↑/↓ arrow keys to select lines you want (it won't highlight everything - it's OK!)
" Shift+i (capital I)
" insert the text you want, e.g. %
" press Esc or EscEsc
" ------------------------------------------------------------------------------------------------------------------
"------------------------------------------------------------------------------------------------------------------
"------------------------------------------------------------------------------------------------------------------
"  gives column no and row no
" set ruler
" set title
"> changing the leader key to .
let mapleader = ","
vmap <leader>y "+y
" nmap <leader>p "+p
" nmap <leader>P "+P
" vmap <leader>p "+p
" vmap <leader>P "+P
" vmap <leader>d "+d
set hlsearch
syntax on

"-------------------------####
" Force saving files that require root permission
cnoremap w!! w !sudo tee > /dev/null %
"-------------------------####
"" With a map leader it's possible to do extra key combinations
"" like <leader>w saves the current file
"let mapleader = " " " Leader is the space key
"let g:mapleader = " "
""auto indent for brackets
"inoremap {<CR> {<CR>}<Esc>O
"" easier write
"nmap <leader>w :w!<cr>
"" easier quit
"nmap <leader>q :q<cr>
"" silence search highlighting
"nnoremap <leader>sh :nohlsearch<Bar>:echo<CR>
""paste from outside buffer
"nnoremap <leader>p :set paste<CR>"+p:set nopaste<CR>
"vnoremap <leader>p <Esc>:set paste<CR>gv"+p:set nopaste<CR>
""copy to outside buffer
"vnoremap <leader>y "+y
""select all
"nnoremap <leader>a ggVG
""paste from 0 register
""Useful because `d` overwrites the <quote> register"}"
""
" hi Search ctermbg=LightYellow
" hi Search ctermfg=Red
" highlight Search cterm=NONE
" highlight Search ctermbg=0
" highlight Search ctermfg=9
" hi QuickFixLine term=reverse ctermbg=52
set cursorline
set backup
set backupdir=~/.vim/.vimtmp/backup//
set directory=~/.vim/.vimtmp/swap//
set noswapfile
set encoding=utf-8
set laststatus=2
">Uncomment to prevent non-normal modes showing in powerline and below powerline.
set noshowmode
set clipboard=unnamed

if has('persistent_undo')
	  set undofile
	    set undodir=~/.vimtmp/undo
	end
" Instead of failing a command because of unsaved changes, instead raise a
" dialogue asking if you wish to save changed files.
" set confirm

"-------------------------------------------------------------------------------------------------------------------
"-------------------------------------------------------------------------------------------------------------------
"---------------------------------------------------------------------------------------------------------------------
" Indentation options {{{1
"
" Indentation settings according to personal preference.

" Indentation settings for using 4 spaces instead of tabs.
" Do not change 'tabstop' from its default value of 8 with this setup.
"> Text, tab and indent related
"set smarttab
set shiftwidth=4
set softtabstop=4
set expandtab
" Indentation settings for using hard tabs for indent. Display tabs as
" four characters wide.
"set shiftwidth=4
"set tabstop=4
" --------------------------------------------------------------------
" When opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent

" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start
" -------------------------------------------------------------------------

set wrap mouse=a
" set mouse=nicr
set foldmethod=manual
" /indent
" set foldlevel=99

"set pastetoggle=<F2>
nnoremap <C-p> :set invpaste paste?<CR>
set pastetoggle=<C-p>
set showmode

"------------------------------
" Completion {{{
  set wildmenu
  set wildmode=longest,full,list

  set ofu=syntaxcomplete#Complete

"<C-n>or<C-p> toshow list
" }}}

"------------------------------------------------------------------------------------------------------------------
">seting the comment toggle----------------------------------------------------------------------------
"------------------------------------------------------------------------------------------------------------------
let s:comment_map = {
    \   "c": '\/\/',
    \   "cpp": '\/\/',
    \   "go": '\/\/',
    \   "java": '\/\/',
    \   "javascript": '\/\/',
    \   "lua": '--',
    \   "scala": '\/\/',
    \   "php": '\/\/',
    \   "python": '#',
    \   "ruby": '#',
    \   "rust": '\/\/',
    \   "sh": '#',
    \   "desktop": '#',
    \   "fstab": '#',
    \   "conf": '#',
    \   "profile": '#',
    \   "bashrc": '#',
    \   "bash_profile": '#',
    \   "mail": '>',
    \   "eml": '>',
    \   "bat": 'REM',
    \   "ahk": ';',
    \   "vim": '"',
    \   "tex": '%',
    \	}


function! ToggleComment()
    if has_key(s:comment_map, &filetype)
        let comment_leader = s:comment_map[&filetype]
        if getline('.') =~ "^\\s*" . comment_leader . " "
            " Uncomment the line
            execute "silent s/^\\(\\s*\\)" . comment_leader . " /\\1/"
        else
            if getline('.') =~ "^\\s*" . comment_leader
                " Uncomment the line
                execute "silent s/^\\(\\s*\\)" . comment_leader . "/\\1/"
            else
                " Comment the line
                execute "silent s/^\\(\\s*\\)/\\1" . comment_leader . " /"
            end
        end
    else
        echo "No comment leader found for filetype"
    end
endfunction


nnoremap <leader>/ :call ToggleComment()<cr>
vnoremap <leader>/ :call ToggleComment()<cr>

"-------------------------------------------------------------------------------------------------------------------------------
"------------------------------------------------------------------------------------------------------------------
"------------------------------------------------------------------------------------------------------------------
" Spelling.
function! ToggleSpell()
  if !exists("b:spell")
    setlocal spell spelllang=en_us
    let b:spell = 1
  else
    setlocal nospell
    unlet b:spell
  endif
endfunction

" Key mappings.
nmap <F4> :call ToggleSpell()<CR>
imap <F4> <Esc>:call ToggleSpell()<CR>
" ------------------------------------
">show search count
"--------------------------------------
set shortmess-=S
"--------------------------
">night-owl
"------------------------


""""" enable the theme

syntax enable
colorscheme night-owl

"---------------tring new cs
"let g:material_theme_style = 'darker-community'|'ocean'|'darker-community'|'palenight'
"let g:material_theme_style = 'palenight'
"let g:material_terminal_italics = 1
"set background=dark

" Palenight
"let g:material_style='palenight'
"set background=dark

"colorscheme material


"----------------------------------
"set termguicolors     " enable true colors support
"let ayucolor="mirage" " for mirage version of theme
"let ayucolor="dark"   " for dark version of theme
"colorscheme ayu


"--------------------
"Cheat plugin

let g:syntastic_javascript_checkers = [ 'jshint' ]
let g:syntastic_ocaml_checkers = ['merlin']
let g:syntastic_python_checkers = ['pylint']
let g:syntastic_shell_checkers = ['shellcheck']

"How to use it
"The easiest way to use this plugin is to use one of the following mappings :
"
"K get answer on the word under the cursor or the selection on a pager (this feature requires vim >= 7.4.1833, you can check if have the right version with : :echo has("patch-7.4.1833"))
"<leader>KK same as K but works on lines or visual selection (not working on neovim, because they killed interactive commands with :!)
"<leader>KB get the answer on a special buffer
"<leader>KR Replace your question by the answer
"<leader>KP Past the answer below your question
"<leader>KC Replay last query, toggling comments
"<leader>KE Send first error to cht.sh
"<leader>C Toggle showing comments by default see configuration
"<leader>KL Replay last query

"The :HowIn command takes exactly one argument.
":Howin javascript open file




"-------------------------------
">airline
"-------------------------------
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
"let g:airline_left_sep = '►'
"let g:airline_right_sep = 'Z '
"let g:airline_left_sep = '>'
"let g:airline_right_sep = '<'
"let g:airline_right_sep = ''
"let g:airline_left_sep = ''

let g:airline_detect_modified=1
let g:airline_detect_paste=1
let g:airline_detect_spell=1
let g:airline_highlighting_cache = 1
"if this doesnt work then install the font from powerline-fonts or just use
"apt to install it
">theme
let g:airline_theme='ayu_mirage'
"let g:airline_minimalist_showmod = 1
" g:airline_theme == 'night_owl'
	" g:airline_theme == 'serene'
	" g:airline_theme == 'ayu_mirage'
	" g:airline_theme == 'minimalist'
	" g:airline_theme == 'wombat'
	" g:airline_theme == 'term'
"raven

">minimap
"----------
">leader key is ","
let g:minimap_show='<leader>ms'
let g:minimap_update='<leader>mu'
let g:minimap_close='<leader>mc'
let g:minimap_toggle='<leader>mt'
"----------------------------------

"line hightlight
"set cursorline
"highlight clear CursorLine
"	highlight CursorLineNR ctermbg=black

"augroup CursorLine
	"  au!
	"    au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
	"	  au WinLeave * setlocal nocursorline
	"  augroup END

":nnoremap <silent> <Leader>l : exe "let m = matchadd('WildMenu','\\%" . line('.') . "l')"<CR>

set number relativenumber
"----------------------
">nerdtree
"----------------------
" Uncomment to autostart the NERDTree
" autocmd vimenter * NERDTree
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeDirArrowExpandable = '►'
let g:NERDTreeDirArrowCollapsible = '▼'
let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=1
let NERDTreeMinimalUI = 1
let g:NERDTreeWinSize=38
"------------------------------------
"----------------
"> VimWiki
"-------------------------
let g:vimwiki_list = [{'path': '~/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]
"---------------------------------------
">syntactic things
"---------------------------------------
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
"----------------------------------



"-----------------------------
">plits and Tabbed Files
"-----------------------------
set splitbelow splitright
"
set path+=**					" Searches current directory recursively.
set wildmenu					" Display all matches when tab complete.
"set nobackup
"set noswapfile
"
let g:minimap_highlight='Visual'

let g:python_highlight_all = 1

au! BufRead,BufWrite,BufWritePost,BufNewFile *.org
au BufEnter *.org            call org#SetOrgFileType()

set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar


"-------------------------------------
"> Trailing spaces
"-------------------------------------
"
function! CleanExtraSpaces() "Function to clean unwanted spaces
    let save_cursor = getpos(".")
        let old_query = getreg('/')
            silent! %s/\s\+$//e
                call setpos('.', save_cursor)
                    call setreg('/', old_query)
                    endfun


 autocmd BufWritePre * :call CleanExtraSpaces()

"---------------------------------------------
"-------------------------------------------
"> Convert text to UTF-8
"-------------------------------------------
setglobal termencoding=utf-8 fileencodings=
scriptencoding utf-8
set encoding=utf-8

autocmd BufNewFile,BufRead  *   try
autocmd BufNewFile,BufRead  *   set encoding=utf-8
autocmd BufNewFile,BufRead  *   endtry

"set everything in utf-8 in every circumtances


"---------------------------------------------------
