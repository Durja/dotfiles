import System.IO
import System.Exit

import XMonad hiding ( (|||) ) -- 'hiding' because there is a conflict with LayoutCombinators
import qualified XMonad.StackSet as W
-----------------DBus---------------------------------
import qualified DBus as D
import qualified DBus.Client as D
-----------------HOOKS---------------------------------
import XMonad.Hooks.SetWMName
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageHelpers(doFullFloat, doCenterFloat, isFullscreen, isDialog, doRectFloat)
import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.DynamicProperty

-----------------CONFIG---------------------------------

import XMonad.Config.Desktop
import XMonad.Config.Azerty
-----------------UTIL---------------------------------
-- import XMonad.Util.NamedActions
import XMonad.Util.NamedScratchpad
import XMonad.Util.EZConfig (additionalKeysP, mkKeymap)
-- additionalKeys, additionalMouseBindings,
-----------------Actions---------------------------------

import XMonad.Actions.CycleWS (shiftTo, WSType(..),toggleWS')
import XMonad.Actions.SpawnOn
-- , nextScreen , prevScreen , shiftNextScreen , shiftPrevScreen
------------------------------------------------

import qualified Codec.Binary.UTF8.String as UTF8
-----------------layout---------------------------------
import XMonad.Layout.LayoutCombinators --Comine Layouts and Jump directly to a Layout
import XMonad.Layout.PerWorkspace (onWorkspace, modWorkspace)
import XMonad.Layout.Spacing
import XMonad.Layout.GridVariants (Grid(Grid)) --Grid Layout
import XMonad.Layout.NoBorders
import XMonad.Layout.MultiToggle as MT
import XMonad.Layout.MultiToggle.Instances (StdTransformers(SMARTBORDERS, NOBORDERS,MIRROR))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import XMonad.Layout.LayoutModifier
import XMonad.Layout.Renamed
import XMonad.Layout.SimplestFloat
import XMonad.Layout.SubLayouts
-- import XMonad.Layout.IndependentScreens
-- import XMonad.Layout.Gaps
-- import XMonad.Layout.ResizableTile
-- import XMonad.Layout.Grid
-- import XMonad.Layout.Fullscreen (fullscreenFull)
-- import XMonad.Layout.Cross(simpleCross)
-- import XMonad.Layout.ThreeColumns
-- import XMonad.Layout.Simplest
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit) --Set limits to the number of windows that can be shown
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..)) --Resize windows in any Layout
import XMonad.Layout.WindowNavigation
-- import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
-- import XMonad.Actions.MouseResize --Resizes windows with the mouse
-- import XMonad.Layout.Minimize
------------------------------------
import Control.Monad (forM_, join, liftM2)
import Graphics.X11.ExtraTypes.XF86
----------------------------------------------------------
-----------------DATA---------------------------------
import qualified Data.Map as M
import qualified Data.ByteString as B
import Data.Monoid
import qualified Data.List        as L

    -- Prompts
-- import XMonad.Prompt
-- import XMonad.Prompt.Shell

-- import XMonad.Util.Run(safeSpawn,spawnPipe)

-----------------------------------

-------------------------------
-----------------------------------
----------------------------------------------

myStartupHook = do
    spawn "$HOME/.xmonad/scripts/autostart.sh"
    setWMName "LG3D"
------------------------------------------------------------------------------------
---------------------------- colours -----------------------------------------------
------------------------------------------------------------------------------------

-- normBord = "#4c566a"
-- focdBord = "#5e81ac"
-- fore     = "#DEE3E0"
-- back     = "#282c34"
-- winType  = "#c678dd"

normBord :: String
normBord   = "#4c566a"  -- Border color of normal windows

focdBord :: String
focdBord  = "#17B3FF"  -- Border color of focused windows

myFont :: String
myFont = "xft:Source Code Variable:Medium:size=9:antialias=true:hinting=true"


--mod4Mask= super key
--mod1Mask= alt key
--controlMask= ctrl key
--shiftMask= shift key

myModMask = mod4Mask
encodeCChar = map fromIntegral . B.unpack
myFocusFollowsMouse = True

myBorderWidth :: Dimension
myBorderWidth = 2

myEditor :: String
myEditor = "subl3"

myBrowser :: String
myBrowser = "vivaldi-stable "

-- myTerminal :: String
myTerminal = "alacritty" -- Sets default terminal
-- alacritty --class scratchpad
---------------------------------------------------------------------------------
--------------------------------------------_TODO--------------------------------
---------------------------------------------------------------------------------
{-
-- COLORS, FONTS, AND PROMPTS {{{

-- <colors>
colorBlack          = "#000000"
colorBlackAlt       = "#040404"
colorGray           = "#444444"
colorGrayAlt        = "#282828"
colorDarkGray       = "#161616"
colorWhite          = "#cfbfad"
colorWhiteAlt       = "#8c8b8e"
colorDarkWhite      = "#606060"
colorCream          = "#a9a6af"
colorDarkCream      = "#5f656b"
colorMagenta        = "#a488d9"
colorMagentaAlt     = "#7965ac"
colorDarkMagenta    = "#8e82a2"
colorBlue           = "#98a7b6"
colorBlueAlt        = "#598691"
colorDarkBlue       = "#464a4a"
colorNormalBorder   = colorDarkWhite
colorFocusedBorder  = colorMagenta

-- <font>
barFont = "-misc-fixed-medium-r-semicondensed-*-12-110-75-75-c-60-koi8-r"

-- <prompts>
myXPConfig :: XPConfig
myXPConfig =
    defaultXPConfig { font                  = barFont
                    , bgColor               = colorDarkGray
                    , fgColor               = colorMagenta
                    , bgHLight              = colorDarkMagenta
                    , fgHLight              = colorDarkGray
                    , borderColor           = colorBlackAlt
                    , promptBorderWidth     = 1
                    , height                = 15
                    , position              = Top
                    , historySize           = 100
                    , historyFilter         = deleteConsecutive
                    }

-- end of COLORS, FONTS, AND PROMPTS }}}-}
{-  -- <prompts/utils>
    , ((modMask,                   xK_semicolon ), shellPrompt myXPConfig)
    , ((modMask .|. shiftMask,     xK_semicolon ), manPrompt myXPConfig)
    , ((modMask .|. controlMask,   xK_semicolon ), xmonadPrompt myXPConfig)
    , ((0,                         xK_F1        ), windowPromptGoto myXPConfig)
    , ((0,                         xK_F2        ), windowPromptBring myXPConfig)
    , ((0,                         xK_F3        ), AL.launchApp myXPConfig "ddg")
    , ((0,                         xK_F4        ), appendFilePrompt myXPConfig "/howl/othe/.TODO_now")
    , ((modMask,                   xK_g         ), goToSelected $ myGSConfig myColorizer)
    , ((modMask .|. shiftMask,     xK_g         ), bringSelected $ myGSConfig myColorizer)
    -}
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------- workspaces -------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
myWorkspaces :: [WorkspaceId]
myWorkspaces  = [ "misc"
                , "web"
                , "term"
                , "dev"
                , "vbox"
                , "gimp"
                , "video"
                -- , "images"
                -- , "file"
                -- , "scratch"
                ]
                --myWorkspaces  = ["www","term","dev","vbox","notes","file"]
                -- myWorkspaces    = ["\61612","\61899","\61947","\61635","\61502","\61501","\61705","\61564","\62150","\61872"]
                --myWorkspaces    = ["","2","3","4","5","6","7","","",""]
                --myWorkspaces    = ["1","2","3","4","5","6","7","8","9","10"]
                --myWorkspaces    = ["I","II","III","IV","V","VI","VII","VIII","IX","X"]
                {---myWorkspaces    = [
                                    "1: \xf07b ", --System
                                    "2: \xf0ac ", --Web
                                    "3: \xf02d ", --Read
                                    "4: \xf0f4 ", --Extra
                                    "5: \xf1fa ", --Chat
                                    "6: \xf11b ", --Game
                                    "7: \xf144 ", --Video
                                    "8: \xf001 ", --Music
                                    "9: \xf019 "  --Sync
                                      ] -- Using Font Awesome-}
---------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------

terminalScratchpad :: NamedScratchpad
terminalScratchpad    = NS "terminal" spawnTerm findTerm manageTerm
    where spawnTerm   = "urxvt -name scratchpad"
          findTerm    = resource  =? "scratchpad"
          manageTerm  = customFloating $ W.RationalRect l t w h
                where
                 h = 0.7
                 w = 0.70
                 t = (0.7) -h
                 l = 0.85 -w

spotifyScratchpad :: NamedScratchpad
spotifyScratchpad       = NS "spotify" spawnspotify queryBool defaultFloating
    where spawnspotify  = "spotify"
          queryBool     = className =? "Spotify"
          floating      = customFloating $ W.RationalRect l t w h
                where
                  h = 0.5025
                  w = 0.01
                  t = 0.4925 -h
                  l = 0.98 -w

cherryScratchpad :: NamedScratchpad
cherryScratchpad    = NS "cherryScratchpad" spawnTerm findTerm manageTerm
    where spawnTerm   = "cherrytree"
          findTerm    = className  =? "Cherrytree"
          manageTerm  = customFloating $ W.RationalRect l t w h
                where
                 h = 0.7
                 w = 0.70
                 t = (0.7) -h
                 l = 0.85 -w

myScratchPads :: [NamedScratchpad]
myScratchPads = [ terminalScratchpad, spotifyScratchpad, cherryScratchpad]


myHandleEventHook :: Event -> X All
myHandleEventHook      = dynamicPropertyChange "WM_NAME" (title =? "Spotify" --> floating)
        where floating = customFloating $ W.RationalRect (1/6) (1/6) (2/3) (2/3)

--------------------------------------------------------------------------------------
myBaseConfig = desktopConfig

---------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------window manipulations/ management-----------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = (composeAll . concat $
    [ [isDialog --> doCenterFloat]
    , [className =? c --> doCenterFloat | c <- myCFloats]
    , [name =? d --> (doRectFloat $ W.RationalRect (1/3) (1/5) (1/2) (4/5)) | d <- myrectF]
    , [title =? t --> doFloat | t <- myTFloats]
    , [resource =? r --> doFloat | r <- myRFloats]
    , [resource =? i --> doIgnore | i <- myIgnores]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "web" | x <- my1Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "term" | x <- my2Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "dev" | x <- my3Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "vbox" | x <- my4Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "gimp" | x <- my5Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "video" | x <- my6Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61612" | x <- my1Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61899" | x <- my2Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61947" | x <- my3Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61635" | x <- my4Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61502" | x <- my5Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61501" | x <- my6Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61705" | x <- my7Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61564" | x <- my8Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\62150" | x <- my9Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61872" | x <- my10Shifts]
    ]) <+> namedScratchpadManageHook myScratchPads
    where
    role      = stringProperty "WM_WINDOW_ROLE"
    name      = stringProperty "WM_NAME"
    -- <<class>>
    -- To find the property name associated with a program, use
    -- -- > xprop | grep WM_CLASS
    -- -- and click on the client you're interested in.

    -- To work on CLI programs, you need to use the window title in 'title' intead of
  -- 'className' as used below.
    myCFloats = ["Arandr", "Arcolinux-tweak-tool.py", "Arcolinux-welcome-app.py", "Galculator", "feh", "mpv", "Xfce4-terminal","arcologout.py","gcolor2"]
    myTFloats = ["Downloads", "Save As...","XDM 2020","Picture in picture"]--title=name
  -- <<resource>>, "Ghidra"
    myRFloats = ["Ghidra", "megasync"]
    myIgnores = ["desktop_window"]
  -- <<name>>
    myrectF   = ["Volume Control"]
    doShiftAndGo = doF . liftM2 (.) W.greedyView W.shift
    my1Shifts = ["qutebrowser", "Vivaldi-stable", "Firefox"]
    -- my2Shifts = []
    my3Shifts = ["subl3"]
    my4Shifts = ["Oracle VM VirtualBox Manager"]
    my5Shifts = ["Gimp", "feh"]
    -- my6Shifts = ["vlc", "mpv"]
    -- my7Shifts = []
    -- my8Shifts = ["Thunar"]
    -- my9Shifts = []
    -- my10Shifts = ["discord"]

---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------- defualtlayout ----------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------

--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

defualtlayout = avoidStruts $ mkToggle (SMARTBORDERS ?? NOBORDERS ?? EOT ) $ grid ||| floats ||| full
                where
                {-tall     = renamed [Replace "tall"]
                           $ windowNavigation ?? MIRROR
                           $ subLayout [] ( Simplest)
                           $ limitWindows 12
                           $ mySpacing 5
                           $ ResizableTall 1 (3/5) (1/2) [0]-}

                grid     = renamed [Replace "grid"]
                           $ windowNavigation
                           $ limitWindows 12
                           $ mySpacing 5
                           $ Grid (16/10)

                floats   = renamed [Replace "floats"]
                           $ windowNavigation
                           $ mySpacing' 4
                           $ limitWindows 20 simplestFloat
                {-monocle = renamed [Replace "monocle"]
                            $ limitWindows 20 Full-}

                full     = renamed [Replace "full"]
                           $ limitWindows 1
                           $ smartBorders (noBorders Full)
----------------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------- mylayout -----------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------

{-myLayout = windowNavigation $
            onWorkspace "vbox" (smartBorders(noBorders Full)) $ defualtlayout
            -- video does full and
            -- onWorkspace "video" (smartBorders(fullL)) $
            -- $ onWorkspace "web" (smartBorders(ThreeColMid 1 (3/100) (1/2) ||| Full))
-}
-----------------------------------------------------------------------------------------------------------------------------------------------------



-----------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------- mouse ----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------
myMouseBindings (XConfig {XMonad.modMask = modMask}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modMask .|. shiftMask, button1), (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    -- , ((modMask, 2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modMask .|. shiftMask, 3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))

    ]
------------------------------------------------------------------------------------------------------------------------------------------------------
{-
 showKeybindings :: [((KeyMask, KeySym), NamedAction)] -> NamedAction
 showKeybindings x = addName "Show Keybindings" $ io $ do
  h <- spawnPipe "zenity --text-info --font=Source Code Variable"
  hPutStr h (unlines $ showKm x)
  hClose h
  return ()-}

-- dont know how to impliment this



-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------ keys config ------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------

myKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
myKeys conf = mkKeymap conf (combKeys conf)

combKeys :: XConfig Layout -> [(String, X ())]
combKeys conf = concatMap ($ conf) keymaps where
    keymaps = [xKeys, spawnKeys, scratches, functionKeys, focusKeys, mediaKeys]

xKeys :: XConfig Layout -> [(String, X ())]
xKeys _ = [-- xmonad --'M'=modkey/super key,'S'=shiftkey,'C'=control,'M1'=ALTkey
          -- recompile n Restarts xmonad
          ("M-S-r", spawn "xmonad --recompile && xmonad --restart")
          , ("M-<Escape>", spawn $ "xkill" )
          , ("M-S-c", kill)
          {-, ("M-C-r", whenX (recompile True) $ do
              broadcastMessage ReleaseResources
              restart "/home/test/.xmonad/xmonad-x86_64-linux" True)-}
          -- restart xmonad
          --, ("M1-r", spawn "xmonad --restart")   
          ]

spawnKeys :: XConfig Layout -> [(String, X ())]
spawnKeys _ = [
              -- SUPER + FUNCTION KEYS
              -- rofi run
               ("M-r", spawn $ "rofi -show run" )
              --, ("M-S r", spawn $ "rofi-theme-selector" )
              -- terminal run
              , ("M-<Return>", spawn $ "alacritty")
              , ("M1-<Return>", spawn $ "urxvt")
              ----------------------------------------------

              , ("M-t p", spawn $ "polybar-msg cmd toggle")
              -- , ("M-c", spawn $ "conky-toggle" )

              ---- Application launcher
              , ("M-x v", spawn $ "vivaldi-stable")
              , ("M-x f", spawn $ "thunar")
              , ("M-<F7>", spawn $ "virtualbox" )
              , ("M-x p", spawn $ "pamac-manager")
              -- xfce appfinder
              , ("M-x a", spawn $ "xfce4-appfinder" )
              -- , ("M-v", spawn $ "pavucontrol" )
              , ("M-x n", spawn $ "nitrogen")
              --Spotify
              , (("M-x s"), spawn $ "env LD_PRELOAD=/usr/lib/spotify-adblock.so spotify %U")

            
              -- ALT + ... KEYS

              -- CONTROL + ALT KEYS
              , ("C-M1-p", spawn $ "$HOME/.xmonad/scripts/picom-toggle.sh")

              --CONTROL + SHIFT KEYS

              , ("C-S-<Escape>", spawn $ "urxvt 'htop task manager' -e htop")
              , ("C-M1-l", spawn $ "arcolinux-logout")
              --mark as favoriate
              ,("M1-f", spawn $ "variety -f" )
              -- go to next
              , ("M1-n", spawn $ "variety -n" )
              -- go to previous
              , ("M1-p", spawn $ "variety -p" )
              -- trash wallpaper
              , ("M1-t", spawn $ "variety -t" )
              {-  --VARIETY KEYS WITH PYWAL

                , ((mod1Mask .|. shiftMask , xK_f ), spawn $ "variety -f && wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&")
                , ((mod1Mask .|. shiftMask , xK_n ), spawn $ "variety -n && wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&")
                , ((mod1Mask .|. shiftMask , xK_p ), spawn $ "variety -p && wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&")
                , ((mod1Mask .|. shiftMask , xK_t ), spawn $ "variety -t && wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&")
                , ((mod1Mask .|. shiftMask , xK_u ), spawn $ "wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&")
              -}


              ]

scratches :: XConfig Layout -> [(String, X ())]
scratches _ = [-- Scratchpads
              ("<F12>", namedScratchpadAction myScratchPads "terminal")
              , ("M-<F5>", namedScratchpadAction myScratchPads "spotify")
              , ("M-<F2>", namedScratchpadAction myScratchPads "cherryScratchpad")
              -- , ("M1-c", namedScratchpadAction myScratchPads "mocp")
              ]

functionKeys :: XConfig Layout -> [(String, X ())]
functionKeys _ =  [
                  --SCREENSHOTS

                  -- , ("C-<Print>", spawn $ "xfce4-screenshooter" )
                  ("<Print>", spawn $ "flameshot gui" )

                  -- ("<Print>", spawn $ "sleep 0.2; scrot -s -e 'xclip -selection clipboard -t \"image/png\" < $f && mv $f ~/Pictures/Screenshots'")

                  -- , ("<Print>", spawn $ "scrot 'ArcoLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'")

                  ]

{-fKeys :: X    Config Layout -> [(String, X ())]
fKeys _ =     [ --F1..12
              -- <F1>
              -- , ("<XF86HomePage>", spawn "firefox")
              -- <F2>
              -- , ("<XF86Mail>", runOrRaise "thunderbird" (resource =? "thunderbird"))
              -- <F3>
              -- , ("<XF86Search>", safeSpawn "firefox" ["https://www.duckduckgo.com/"])
              -- <F4>
              -- , ("<XF86Calculator>", runOrRaise "qalculate-gtk" (resource =? "qalculate-gtk"))
              -- , ( "<F12>", spawn $ "xfce4-terminal --drop-down" )
              {--- Increase brightness
              , ("<xF86MonBrightnessUp>",  spawn $ "xbacklight -inc 5")
              -- Decrease brightness
              , ("<XF86MonBrightnessDown>", spawn $ "xbacklight -dec 5")
              -}
              ]-}

mediaKeys :: XConfig Layout -> [(String, X ())]
mediaKeys _ = [--mocp terminal player
              {-("<XF86AudioPlay>", spawn $ "mocp -G && notify-send \"$(mocp -Q %artist)\" \"$(mocp -Q %song)\" -i deepin-music-player")
              , ("<XF86AudioNext>", spawn $ "mocp -f && notify-send \"$(mocp -Q %artist)\" \"$(mocp -Q %song)\" -i deepin-music-player")
              , ("<XF86AudioPrev>", spawn $ "mocp -r && notify-send \"$(mocp -Q %artist)\" \"$(mocp -Q %song)\" -i deepin-music-player")-}
               ("<XF86AudioPlay>", spawn $ "playerctl play-pause")
              , ("<XF86AudioNext>", spawn $ "playerctl next")
              , ("<XF86AudioPrev>", spawn $ "playerctl previous")

              --  -- Mute volume <F9>
              , ("<XF86AudioMute>", spawn $ "amixer -q set Master toggle")

              -- Increase volume <F10>
              , ("<XF86AudioRaiseVolume>", spawn $ "amixer -q set Master 5%+")

              -- Decrease volume <F11>
              , ("<XF86AudioLowerVolume>", spawn $ "amixer -q set Master 5%-")
              ]

toggleFloat w = windows (\s -> if M.member w (W.floating s)
                then W.sink w s
                else (W.float w (W.RationalRect (1/6) (1/6) (2/3) (2/3)) s))

focusKeys  :: XConfig Layout -> [(String, X ())]
focusKeys conf =  [
                  -- Move focus to the previous window.
                  ("M-k", windows W.focusUp )
                  -- Move focus to the next window.
                  , ("M-j", windows W.focusDown)
                  -- Swap the focused window with the next window.
                  -- , ("M-S-j", windows W.swapDown  )

                  -- Swap the focused window with the previous window.
                  -- , ("M-S-k", windows W.swapUp    )
                 {-this is probably for resizabletiles
                  -- Shrink the master area.
                  , ("M-S-h", sendMessage Shrink)

                  -- Expand the master area.
                  , ("M-S-l", sendMessage Expand)
                  -- increase or decrease number of windows in the master area
                  , ("M-,", sendMessage (IncMasterN 1))
                  , ("M-.", sendMessage (IncMasterN (-1)))
                  -- change layout
                  -- , ("M-S-,", sendMessage (IncMasterN (-1)))
                  -- , ("M-S-.", sendMessage (IncMasterN 1))
                  -}

                  -- Rotate all windows except master and keep focus in place
                  -- , ("M-S-<Tab>", rotSlavesDown)
                  -----------------------------------
                  -----------------------------------
                  -- Rotate all the windows in the current stack
                  -- , ("M-C-<Tab>", rotAllDown)
                   -- jump directly to the Full layout
                   , ("M-<Space>", sendMessage $ JumpToLayout "full")
                  {--- Switch to next layout
                  , ("M-`", sendMessage NextLayout)-}
                  --  Reset the layouts on the current workspace to default.
                  , ("M-S-<Space>", setLayout $ XMonad.layoutHook conf)
                  -- Switch to next layout
                  , ("M-<Tab>", toggleWS' ["NSP"])
                  -- A way to get to the hidden named scratchpad workspace
                  , ("M-n", windows $ W.greedyView "NSP")
                  -- Move focus to the master window
                  , ("M-m", windows W.focusMaster)
                  -- moving window to next empty ws
                  , ("M-C-<Space>",  shiftTo Next EmptyWS)
                  --makes window float and sinks back to place
                  , ("M-t f", withFocused toggleFloat)
                  -- Toggles noborder/full if there are no border then this works
                  -- , ("M-<Space>", sendMessage (MT.Toggle SMARTBORDERS) >> sendMessage ToggleStruts)
                  -- , ("M-d", withFocused $ windows . W.sink)
                  , ("M-t m", sendMessage (MT.Toggle SMARTBORDERS))
                  ]
                  ++
                  -- moves between workspaces
                  [("M-" ++ show k, windows $ W.greedyView i) | (k, i) <- zip [1..9] (XMonad.workspaces conf)]
                  ++
                  -- sends only window to ws list REVERSED:L.reverse $ XMonad.workspaces conf
                  [("M-C-" ++ show k, windows $ W.shift i) | (k, i) <- zip [1..9] (L.reverse $ XMonad.workspaces conf)]
                  ++
                  -- moves with window between Workspaces
                  [("M-S-" ++ show k, windows $ W.greedyView i . W.shift i) | (k, i) <- zip [1..9] (XMonad.workspaces conf)]

{-    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm,button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))
-}


main :: IO ()
main = do

    dbus <- D.connectSession
    -- Request access to the DBus name
    D.requestName dbus (D.busName_ "org.xmonad.Log")
        [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]


    xmonad . ewmh $
            myBaseConfig

                {startupHook = myStartupHook
, layoutHook          = defualtlayout ||| layoutHook myBaseConfig
, manageHook          = manageSpawn <+>
                        myManageHook <+>
                        manageHook myBaseConfig

, modMask             = myModMask
, borderWidth         = myBorderWidth
, handleEventHook     = handleEventHook myBaseConfig <+>
                        fullscreenEventHook <+>
                        myHandleEventHook

, focusFollowsMouse   = myFocusFollowsMouse
, workspaces          = myWorkspaces
, focusedBorderColor  = focdBord
, normalBorderColor   = normBord
, keys                = myKeys
, mouseBindings       = myMouseBindings
}





